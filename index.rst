.. The GIST Pipeline documentation master file, created by
   sphinx-quickstart on Wed Jun  5 09:51:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to The GIST Pipeline's documentation!
=============================================

The GIST is a convenient, all-in-one framework for the scientific analysis of fully reduced, (integral-field)
spectroscopic data. It is entirely written in Python3 and conducts all steps from the preparation of input data, over
the scientific analysis to the production of publication-quality plots.

In its default implementation, it extracts stellar kinematics, performs an emission-line analysis, derives star
formation histories and stellar population properties from full spectral fitting as well as via the measurement of
absorption line-strength indices. To this end, the GIST is exploiting the well-known pPXF and GandALF routines. In
addition, the framework is not specific to any instrument or analysis technique and provides easy means of modification
and further development, as of its modular code architecture. In fact, it is not only a neat combination of already
existing fitting routines, but a fully modular framework for the analysis of spectroscopic data in the context of a
variety of scientific objectives. 

The software further features the dedicated visualisation routine Mapviewer which has a sophisticated graphical user
interface. This allows the easy, fully-interactive plotting of all measurements, in particular maps, observed spectra,
fits, residuals, as well as star formation histories and the weight distribution of the models. 

An elaborate, Python-native parallelisation is implemented and tested on various machines from laptop to cluster scales. 

To date, the GIST framework has successfully been applied to both low and high-redshift data from MUSE, PPAK (CALIFA),
SINFONI, KCWI, and MaNGA, as well as to simulated data for HARMONI, WEAVE, and other artificial observations. 



|

.. admonition:: Contact

   The GIST pipeline and this documentation have so far been maintained by Adrian Bittner. Having left academia in 
   September 2021, further support and maintenance of the software will be limited, but in case of doubt you can get in 
   contact via :email:`gistpipeline@gmail.com<gistpipeline@gmail.com>`



.. attention:: 
   Please do not forget to cite Bittner et al. (`2019 <https://ui.adsabs.harvard.edu/abs/2019arXiv190604746B/abstract>`_)
   and the papers describing the underlying analysis routines if you use this software in any publication. 



|

GitLab Repository of the GIST
-----------------------------

You can access the source code of the GIST and its previous releases directly in its official GitLab repository. You are 
also welcome to open issues there. 

* `GitLab Repository <https://gitlab.com/abittner/gist-development>`_



|


.. toctree::
   :maxdepth: 1
   :caption: Contents

   documentation/intro/introduction
   documentation/generalRemarks/generalRemarks
   documentation/download/download
   documentation/usage/usage
   documentation/modules/modules
   documentation/visualisation/visualisation
   documentation/final_remarks/final_remarks
   documentation/contact/contact



|

