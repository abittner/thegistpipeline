.. _acknowledgements:

======================
Acknowledgements
======================

The authors thank Harald Kuntschner and Michele Cappellari for their permission to distribute their codes together with
this software package. We further thank Alexandre Vazdekis for permission to include the `MILES
<http://miles.iac.es>`_ library.  The pipeline makes use of `Astropy <http://www.astropy.org>`_, a community-developed
core Python package for Astronomy (Astropy Collaboration et al. `2013
<https://ui.adsabs.harvard.edu/#abs/2013A&A...558A..33A>`_, `2018
<https://ui.adsabs.harvard.edu/#abs/2018AJ....156..123A>`_), as well as `NumPy <http://www.numpy.org>`_, `SciPy
<https://www.scipy.org>`_ and `Matplotlib <https://matplotlib.org>`_. 


