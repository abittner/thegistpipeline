.. _bugs_future:

==================================
Known Bugs and Upcoming Features
==================================

Upcoming Features
--------------------

* An alternative implementation of the ``starFormationHistories`` module exploiting the FIREFLY routine (`Wilkinson et
  al., 2017 <https://ui.adsabs.harvard.edu/abs/2017MNRAS.472.4297W/abstract>`_) is currently under development. 



|

Known Bugs
--------------------

* Please note that the first release of the pyGandALF routine (`pyGandALF-webpage <http://star-www.herts.ac.uk/~sarzi/>`_)
  supports the calculation of errors on the emission-line fits only in Python 2.7. This implies that this error estimation
  will automatically be skipped if you execute the GIST pipeline, as it requires Python 3. Note that the developers of
  pyGandALF are aware of this issue and we expect to solve this problem shortly.  

* PyQt5 (which is required to execute the *Mapviewer*) seems to incompatible with macOS<=10.9. The only solution seems
  to be to update the OS. 

* In some specific cases it might be helpful to lower the tolerance (variable ``ftol``) of the fitting in Gandalf. 



|

