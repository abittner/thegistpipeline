.. _gasModule:

``emissionLines`` - Derive emission-line properties
===================================================



Purpose 
...............

This module performs a full emission-line analysis of the observed spectra. The GIST is
currently equipped with one routine that can readily be used by setting the configuration parameter ``GAS|METHOD`` to
``GANDALF``. In particular, this routine employs a Python translation of the original GandALF routine by 
`Sarzi et al. (2006) <https://ui.adsabs.harvard.edu/?#abs/2006MNRAS.366.1151S>`_ and `Falcon-Barroso et al. (2006)
<https://ui.adsabs.harvard.edu/abs/2006MNRAS.369..529F>`_. 

Note that the routine can be run on ``BIN`` and ``SPAXEL`` level data (configuration parameter ``GAS|LEVEL``). With both
these options, the analysis is performed using the full set of spectral templates.  Alternatively, with the
configuration parameter ``BOTH``, a bin-level run using the full set of spectral templates is performed. Thereafter, a
spaxel-level run is performed, using for each spaxel the optimal template derived in the bin-level run (of the bin the
corresponding spaxel belongs to) instead of the full set of spectral templates. 



|

Output
...............

Files
++++++++++

All outputs can be available at the ``BIN`` and ``SPAXEL`` level. 

- ``_gas_BIN.fits``: Extension 1: 

   - **Columns**: [LineName]_[LineWavelength]_*: ``F`` Flux; ``A`` Amplitude; ``V`` Velocity; ``S`` Sigma; ``AON`` Amplitude-to-noise ratio \| ``EBmV_0`` Screen-like extinction from the stellar continuum \| ``EBmV_1`` Extinction in emission-line regions from Balmer decrement
   - **Rows**: *One line per bin/spaxel.*

- ``_gas_BIN.fits``, Extension 2: 

   - **Columns**: Same as in extension 1, but for errors
   - **Rows**: *One line per bin/spaxel.*

- ``_gas-bestfit_BIN.fits``, Extension 1: 

   - The best fit to the spectrum, including continuum and emission lines
   - **Columns**: ``BESTFIT`` The best fit to the spectrum
   - **Rows**: *One fit per bin/spaxel.*

- ``_gas-bestfit_BIN.fits``, Extension 2: 

   - **Columns**: ``LOGLAM`` The corresponding wavelength array

- ``_gas-bestfit_BIN.fits``, Extension 3:

   - **Columns**: ``GOODPIX`` The spectral pixels included in the fit

- ``_gas-emssion_BIN.fits``: The emission spectra

   - **Columns**: ``EMISSION`` The emission spectra
   - **Rows** *One fit per bin/spaxel.*

- ``_gas-cleaned_BIN.fits``: Extension 1:

   - **Columns**: ``SPEC`` Emission-line subtracted spectra
   - **Rows**: *One line per bin/spaxel.*

- ``_gas-cleaned_BIN.fits``: Extension 2:

   - **Columns**: ``LOGLAM``: The corresponding wavelength array

- ``_gas-weights_BIN.fits``: Extension 1:

   - **Columns**: ``NWEIGHTS`` Normalized weights assigned to each template during the fit
   - **Rows**: *One line per bin/spaxel.*

- ``_gas-weights_BIN.fits``: Extension 2:

   - **Columns**: ``EWEIGHTS`` Weights of emission lines
   - **Rows**: *One line per bin/spaxel.*

- ``_gas-optimalTemplate_BIN.fits``: Extension 1:

   - **Columns**: ``OPTIMAL_TEMPLATE`` Optimal templates 
   - **Rows**: *One template per bin/spaxel.*

- ``_gas-optimalTemplate_BIN.fits``: Extension 2:

   - **Columns**: ``LOGLAM_TEMPLATE`` The corresponding wavelength array


Return Statement
++++++++++++++++

The module should end with ``return(None)``.  If an uncaught exception occurs in the module, all following modules will
be skipped for this galaxy. If you intend to manually skip all subsequent modules, simply raise an exception. 



| 

