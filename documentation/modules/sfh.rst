.. _sfhModule:

``starFormationHistories`` - Derive star formation histories
============================================================



Purpose
...............

The ``starFormationHistories`` module derives non-parametric star formation histories from the observed spectra. The
GIST framework is currently equipped with one routine that can readily be used by setting the configuration parameter
``SFH|METHOD`` to ``PPXF``. In particular, this routine employs the PPXF routine of `Cappellari & Emsellem (2004)
<https://ui.adsabs.harvard.edu/?#abs/2004PASP..116..138C>`_. 

In addition, a ``starFormationHistories``-module employing the FIREFLY routine is currently in development. 



|

Output
...............

Files
++++++++++

- ``_sfh.fits``:

   - **Columns**: ``AGE`` Age \| ``METAL`` Metallicity \| ``ALPHA`` Alpha enhancement \| ``V`` ``SIGMA`` ``H3`` ``H4`` ``H5`` ``H6`` Stellar kinematics \| ``FORM_ERR_*`` Formal errors on the stellar kinematics
   - **Rows**: *One line per bin.*

- ``_sfh-weights.fits``: The weights assigned to each template during the fit

   - **Columns**: ``WEIGHTS`` The weights
   - **Rows**: *One line per bin.*

- ``_sfh-bestfit.fits``, Extension 1: 

   - The best fit to the spectrum
   - **Columns**: ``BESTFIT`` The best fit to the spectrum
   - **Rows**: *One fit per bin.*

- ``_sfh-bestfit.fits``, Extension 2: 

   - **Columns**: ``LOGLAM`` The corresponding wavelength array

- ``_sfh-bestfit.fits``, Extension 3: 

   - **Columns**: ``GOODPIX`` The spectral pixels included in the fit


Return Statement
++++++++++++++++

The module should end with ``return(None)``.  If an uncaught exception occurs in the module, all following modules will
be skipped for this galaxy. If you intend to manually skip all subsequent modules, simply raise an exception. 



| 

