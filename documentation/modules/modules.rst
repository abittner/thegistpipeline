.. _modules: 

*****************
The Modules
*****************


In this section I summarise the workflow of the GIST framework and its modules, the default implementations, and how
users can develop and use their own modules. 


.. toctree::
   :maxdepth: 1
   
   overview
   readData
   spatialMasking
   spatialBinning
   prepareSpectra
   kin
   gas
   sfh
   ls
   prepareTemplates
   auxiliary
