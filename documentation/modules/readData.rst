.. _readDataModule:

``readData`` - Read-in the data
===============================



Purpose
...............

Read the input data, for instance the IFU cube, and return the relevant information in a dictionary. Such a read-in
routine has to perform the following tasks: 

* Read spectra and error spectra
* Get the wavelength information
* Get the spatial coordinates
* Shift the spectra to restframe, according the configuration parameter ``GENERAL|REDSHIFT`` (Note that this is required for user-defined read-in routines, in order to work consistently with the subsequent modules). 
* Shorten the wavelength range of the spectra
* Compute a signal-to-noise ratio for each spaxel
* Determine the velscale of the spectra

| 

The GIST pipeline is equipped with read-in routines for the following instruments: 

* ``MUSE_WFM``: The wide-field-mode of the MUSE spectrograph *without* adaptive optics
* ``MUSE_WFMAON``: The wide-field-mode of the MUSE spectrograph *with* adaptive optics, using the nominal wavelength range
* ``MUSE_WFMAOE``: The wide-field-mode of the MUSE spectrograph *with* adaptive optics, using the extended wavelength range
* ``MUSE_NFM``: The narrow-field-mode of the MUSE spectrograph *without* adaptive optics
* ``MUSE_NFMAO``: The narrow-field-mode of the MUSE spectrograph *with* adaptive optics
* ``CALIFA_V500``: The V500 resolution mode of CALIFA
* ``CALIFA_V1200``: The V1200 resolution mode of CALIFA
* ``PLAINTXT``: Single spectrum in plain text format (wavelength in 1st column, spectrum in 2nd column)

These read-in routines are included in the source code of the pipeline and can readily be used by setting the
configuration parameter ``READ_DATA|METHOD`` to above values. 

Note that the MUSE read-in routines with and without adaptive optics are identical, except that the adaptive optics
routines ignore the wavelength range affected by the AO lasers (5780A to 6048A). 



|

Output
...............

Files 
+++++++

The GIST framework does not expect any output to be saved to disk. 


Function Returns
+++++++++++++++++

Any read-in routines must return a dictionary containing the following information:

* ``x``         x-coordinate for each spaxel [array, size=(nspaxel,)]
* ``y``         y-coordinate for each spaxel [array, size=(nspaxel,)]
* ``wave``      Wavelength array [array, size=(nspecs,)]
* ``spec``      Spectra [array, size=(nspecs,nspaxel)]
* ``error``     Error spectra [array, size=(nspecs,nspaxel)]
* ``snr``       Signal-to-noise ratio for each spaxel [array, size=(nspaxel,)]
* ``signal``    Signal in each spaxel [array, size=(nspaxel,)]
* ``noise``     Noise in each spaxel [array, size=(nspaxel,)]
* ``velscale``  Spectral pixelsize in velocity space [float]
* ``pixelsize`` Spatial pixelsize of the instrument [float]

(nspaxel denotes the total number of spatial pixels; nspecs denotes the total number of spectral pixels)



|

