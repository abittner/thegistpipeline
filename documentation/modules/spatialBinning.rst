.. _spatialBinningModule:

``spatialBinning`` - Spatially bin the data
===========================================



Purpose
...............

The purpose of the module is to spatially bin the data, for instance in order to obtain an approximately constant
signal-to-noise ratio throughout the field-of-view. Alternatively, one could use this step in the workflow to isolate
spectra of specific features, e.g. globular cluster or planetary nebulae, and continue the analysis only with these. In
this way the GIST could simply be turned into a specific tool for the analysis of globular clusters of planetary
nebulae. 

The GIST is currently equipped with one routine to spatially bin the data. This routine is included in the source and
exploits the adaptive Voronoi tesselation routine of `Cappellari & Copin (2003)
<https://ui.adsabs.harvard.edu/?#abs/2003MNRAS.342..345C>`_. It can readily be used by setting the configuration
parameter ``SPATIAL_BINNING|METHOD`` to ``VORONOI``. 

In addition, the module determines the closest valid Voronoi bin for all masked spaxels. These are assigned a negative
``BIN_ID``, with the absolute value stating the ``BIN_ID`` of the closest valid Voronoi bin. A ``BIN_ID`` 0 is a valid
Voronoi bin, in particular the one containing the spaxel with the highest signal-to-noise ratio.  

The routine finally saves a tables that defines which spaxel belongs to which bin, including basic properties of bins
and spaxels, such as position, flux, signal-to-noise, etc.



|

Output
...............

Files
++++++++++

* ``_table.fits``: Contains information to reconstruct the spatial binning pattern. Most importantly, it defines the ``BIN_ID``, that assigns each spaxel to a bin.

   - **Columns**: ``ID`` Spaxel ID \| ``BIN_ID`` Bin ID \| ``X`` ``Y`` Pixel coordinates of spaxels \| ``XBIN`` ``YBIN`` Pixel coordinates of bins \| ``FLUX`` Flux in the spaxel \| ``SNR`` Signal-to-noise ratio in the spaxel \| ``SNRBIN`` Signal-to-noise ratio in the bin \| ``NSPAX`` Number of spaxels in the bin
   - **Rows**: *One line per spaxel.*
   - **Header**: The header of extension 0 has to contain the entry ``PIXSIZE`` defining the angular size of one spatial pixel.


Function Returns
++++++++++++++++

The module should end with ``return(None)``.  If an uncaught exception occurs in the module, all following modules will
be skipped for this galaxy. If you intend to manually skip all subsequent modules, simply raise an exception. 



|

