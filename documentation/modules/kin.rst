.. _kinematicsModule:

``stellarKinematics`` - Derive stellar kinematics
=================================================



Purpose
...............

The ``stellarKinematics`` module measures the stellar kinematics of the observed spectra. The GIST is currently equipped
with one routine that can readily be used by setting the configuration parameter ``KIN|METHOD`` to ``PPXF``. In
particular, this routine employs the PPXF routine of `Cappellari and Emsellem (2004)
<https://ui.adsabs.harvard.edu/?#abs/2004PASP..116..138C>`_.
More precisely: 

* Initial guesses: The used initial guess for the stellar velocity is 0 (as all spectra have been shifted to
  rest-frame). The initial guess for the velocity dispersion is defined in the configuration parameter ``KIN|SIGMA``.
  Alternatively, one can define initial guesses for all bins separately, i.e. by supplying the file ``*_kin-guess.fits``
  in the output directory before the run. This file must contain a column ``V`` and ``SIGMA`` (as in the output file
  ``*_kin.fits``, defining an initial guess for each spatial bin. 
* A spectral mask is applied, according to the masking file defined by ``KIN|SPEC_MASK``. See :ref:`configuration` for
  details. 
* The module calculates the lambda_r parameter (a proxy for the projected, specific angular momentum) for each spatial
  bin separately. 



|

Output
...............

Files
++++++++++

- ``_kin.fits``: Results of the extraction of stellar kinematics and their errors
  
   - **Columns**: ``V`` ``SIGMA`` ``H3`` ``H4`` ``H5`` ``H6`` Stellar kinematics \| ``ERR_*`` Errors on the stellar kinematics from MC-simulations \| ``FORM_ERR_*`` Formal errors on the stellar kinematics \| ``LAMBDA_R`` Lambda_R parameter as proxy for the stellar angular momentum, calculated for each spatial bin separately \| ``REDDENING`` Extinction calculated by PPXF
   - **Rows**: *One line per bin.*
   
- ``_kin-bestfit.fits``, Extension 1:   

   - The best fit to the spectrum
   - **Columns**: ``BESTFIT`` The best fit to the spectrum
   - **Rows**: *One fit per bin.*

- ``_kin-bestfit.fits``, Extension 2:
  
   - **Columns**: ``LOGLAM`` The corresponding wavelength array

- ``_kin-bestfit.fits``, Extension 3: 

   - **Columns**: ``GOODPIX`` The spectral pixels included in the fit

- ``_kin-optimalTemplates.fits``: Extension 1

   - Optimal template for the fit on each bin.   
   - **Columns**: ``OPTIMAL_TEMPLATES`` Optimal templates
   - **Rows**: *One template per bin.*

- ``_kin-optimalTemplates.fits``: Extension 2

   - **Columns**: ``LOGLAM_TEMPLATE`` The corresponding wavelength array


Function Returns
++++++++++++++++

The module should end with ``return(None)``.  If an uncaught exception occurs in the module, all following modules will
be skipped for this galaxy. If you intend to manually skip all subsequent modules, simply raise an exception. 



| 

