.. _lsModule:

``lineStrengths`` - Measure line strength indices
=================================================



Purpose
...............

The ``lineStrengths`` module measures line strength indices from the observed spectra. The GIST is currently equipped
with one routine that can readily be used by setting the configuration parameter ``LS|METHOD`` to ``default``.  In
particular, this routine employs a Python translation of the line strength measurement routine of `Kuntschner et al.
(2006) <https://ui.adsabs.harvard.edu/?#abs/2006MNRAS.369..497K>`_ and the routine of `Martin-Navarro et al. (2018)
<https://ui.adsabs.harvard.edu/#abs/2018MNRAS.475.3700M>`_ in order to match the measured line strength indices to
single stellar population equivalent population properties. 



|

Output
...............

Files
++++++++++

All outputs are available in the ``OrigRes`` and ``AdapRes`` versions, as derived at the original and adapted spectral
resolution of the spectra. 

- ``_ls.fits``, Extension 1: Line strength indices and their errors as estimated from MC-simulations

   - **Columns**: ``Fe5015`` Line strength index, names as defined in ``lsBands.config`` \| ``ERR_*`` Errors on the line strength indices as estimated from MC-simulations
   - **Rows**: *One line per bin*

- ``_ls.fits``, Extension 2: Single stellar population equivalent population properties as estimated with a MCMC algorithm from the line strength indices

   - **Columns**: ``[lineLabel]_PERCENTILES`` Percentiles of the resulting MCMC distribution for the line denoted [lineLabel] in ``lsBands.config``
   - **Rows**: *One line per bin*

- ``_ls-cleaned_linear.fits``: Extension 1:

   - Emission-subtracted, linearly binned spectra
   - **Columns**: ``SPEC`` Emission-subtracted, linearly binned spectra \| ``ESPEC`` Variance spectra
   - **Rows**: *One line per bin.*

- ``_ls-cleaned_linear.fits``: Extension 2:

   - **Columns**: ``LAM`` The corresponding wavelength array


Return Statement
++++++++++++++++

The module should end with ``return(None)``.  If an uncaught exception occurs in the module, all following modules will
be skipped for this galaxy. If you intend to manually skip all subsequent modules, simply raise an exception. 



| 
     
