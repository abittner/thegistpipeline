.. _moduleOverview:

=======================
Overview of the modules
=======================



The GIST Framework
-----------------------

The GIST pipeline is not only a neat combination of already existing fitting routines, but a fully modular framework for
the analysis of spectroscopic data. This framework consists of a series of modules which conduct all necessary steps
from the read-in of input data, over its preparation and fitting, to the visualisation of the results. 

.. figure:: ../../_images/flowchart.pdf
   :align: right
   :figwidth: 50%

The figure on the right illustrates the workflow of the GIST pipeline: The main sequence of tasks constitutes of 8
modules that are always executed in this order. If the output of a module is already available in the output directory,
the module is not executed again. Instead, the available results are being used by the subsequent modules.  Note that
this behaviour can be turned off in the ``MasterConfig`` file. 

Two modules are detached from this main workflow. This is necessary as the functions implemented in ``auxiliary`` and
``prepareTemplates`` are accessed by various modules in the main workflow. Naturally, the same applies to the
visualisation modules. 



|

User-developed modules 
-----------------------

This highly modular design of the GIST framework makes it straightforward for the user to replace the modules by
user-developed implementations. For instance, one could easily implement an alternative routine that uses FIREFLY or
STECKMAP instead of pPXF for the derivation of star formation histories, or pyPARADISE instead of pyGandALF for the
emission-line analysis, without affecting any other parts of the analysis framework. **In fact, a FIREFLY
implementation is currently in development.** Similarly, one could replace the Voronoi binning routine in the
``spatialBinning`` module by a script that, for instance, extracts the spectra of globular clusters or planetary nebulae
and only propagates these to the subsequent modules. This way, the GIST can easily be turned into a specialised tool for
the analysis of globular clusters and planetary nebulae. 



|

Requirements
............

To this end, the user has to simply write a routine that performs the necessary tasks, place it in the subdirectory of
the corresponding module in the source code, reinstall the GIST, and specify this new routine and its configuration
parameters in the ``MasterConfig`` file. 

All user-developed modules should have a main function that has the same name as in the default routines, accepts the
same arguments, and ends with the same return statement. Please consider the default routines for each module as
template. 

For full compatibility with subsequent GIST modules and the Mapviewer, any user-developed routines should produce the
same outputs as the default GIST implementations described in the following pages.  Of course, any routine can generate
additional outputs that are not used/expected by other parts of GIST. Note that it is possible to add additional columns
to the default output files (e.g. in ``_kin.fits``). Such additional columns will not affect other analysis modules and
will readily be available for plotting in the Mapviewer.

Naturally, the user is not restricted in loading additional input files in their routines. 



| 

Adapting the MasterConfig file
..............................

Different implementations of the same module (e.g. using FIREFLY instead of pPXF) might require different sets of
configuration parameters. These configuration parameters can naturally be passed via the ``MasterConfig`` file. To this
end, the ``MasterConfig`` file is split in various sections, each corresponding to one module in the GIST workflow.
Within each section, only the ``METHOD`` keyword is required (except the ``GAS|LEVEL`` keyword which is required
as well). This keyword states which routine/implementation is to be used to execute a module in the workflow. The
remaining keywords in the section are only used by the selected routine and not by any other part of the GIST pipeline.
Therefore, one can naturally add/remove/change the parameters in the ``MasterConfig`` file to comply with the needs of
the user-developed routine. 

If the user-developed routine requires more complicated configurations (e.g. similar to the ``emissionLines.config``
file used for pyGandALF), I recommend to specify its file path in the ``MasterConfig`` file and subsequently read and
use this file from within the user-build module. 

General information on the current run of the GIST framework is available in the section ``GENERAL`` of the
``MasterConfig`` file. The information stored there can be used by any module, but must not be changed throughout the
analysis. 



|

