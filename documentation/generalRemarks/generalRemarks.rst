.. _generalRemarks: 

.. highlight:: bibtex

*****************
General Remarks
*****************


Citing this Software Package
--------------------------------

If you use this software package for any publication, please cite the paper `Bittner et al. 2019
<https://ui.adsabs.harvard.edu/abs/2019arXiv190604746B>`_  and mention this webpage in a footnote. 
The BibTeX entry for this publication is::

  @ARTICLE{2019A&A...628A.117B,
         author = {{Bittner}, A. and {Falc{\'o}n-Barroso}, J. and {Nedelchev}, B. and {Dorta}, A. and {Gadotti}, D.~A. and {Sarzi}, M. and {Molaeinezhad}, A. and {Iodice}, E. and {Rosado-Belza}, D. and {de Lorenzo-C{\'a}ceres}, A. and {Fragkoudi}, F. and {Gal{\'a}n-de Anta}, P.~M. and {Husemann}, B. and {M{\'e}ndez-Abreu}, J. and {Neumann}, J. and {Pinna}, F. and {Querejeta}, M. and {S{\'a}nchez-Bl{\'a}zquez}, P. and {Seidel}, M.~K.},
          title = "{The GIST pipeline: A multi-purpose tool for the analysis and visualisation of (integral-field) spectroscopic data}",
        journal = {\aap},
       keywords = {methods: data analysis, techniques: spectroscopic, galaxies: individual: NGC 1433, galaxies: stellar content, galaxies: kinematics and dynamics, galaxies: structure, Astrophysics - Astrophysics of Galaxies, Astrophysics - Instrumentation and Methods for Astrophysics},
           year = "2019",
          month = "Aug",
         volume = {628},
            eid = {A117},
          pages = {A117},
            doi = {10.1051/0004-6361/201935829},
  archivePrefix = {arXiv},
         eprint = {1906.04746},
   primaryClass = {astro-ph.GA},
         adsurl = {https://ui.adsabs.harvard.edu/abs/2019A&A...628A.117B},
        adsnote = {Provided by the SAO/NASA Astrophysics Data System}
  }


|

Citing the Analysis Techniques
--------------------------------

We remind the user to also cite the publications of the underlying analysis techniques and models, if these are
used in the analysis. These are in particular: 

 * **Voronoi-binning method:** `Cappellari & Copin 2003 <https://ui.adsabs.harvard.edu/?#abs/2003MNRAS.342..345C>`_
 * **Penalized Pixel-Fitting (pPXF) method:** `Cappellari & Emsellem 2004 <https://ui.adsabs.harvard.edu/?#abs/2004PASP..116..138C>`_; `Cappellari 2017 <https://ui.adsabs.harvard.edu/?#abs/2017MNRAS.466..798C>`_
 * **GandALF and pyGandALF routines:** `Sarzi et al. 2006 <https://ui.adsabs.harvard.edu/?#abs/2006MNRAS.366.1151S>`_; `Falcon-Barroso et al. 2006 <https://ui.adsabs.harvard.edu/abs/2006MNRAS.369..529F>`_; `Bittner et al. 2019 <https://ui.adsabs.harvard.edu/abs/2019arXiv190604746B>`_
 * **LS-Measurements routines:** `Kuntschner et al. 2006 <https://ui.adsabs.harvard.edu/?#abs/2006MNRAS.369..497K>`_; `Martin-Navarro et al. 2018 <https://ui.adsabs.harvard.edu/#abs/2018MNRAS.475.3700M>`_
 * **Miles models:** `Vazdekis et al. 2010 <https://ui.adsabs.harvard.edu/abs/2010MNRAS.404.1639V>`_

|

.. warning::
   Although we provide this pipeline as a convenient, all-in-one framework for the analysis of IFS data, it is of
   fundamental importance that the user understands exactly how the involved analysis methods work. We warn that the
   improper use of any of these analysis methods, whether executed within the framework of the **GIST** pipeline or not,
   will likely result in spurious or erroneous results and their proper use is solely the responsibility of the user.
   Likewise, the user should be fully aware of the properties of the input data before intending to derive high-level
   data products. Therefore, this pipeline should not be simply adopted as a black-box. To this extend, we urge any user
   to get familiar with both the input data and analysis methods, as well as their implementation in this pipeline. 

|

