=====================
Contact
=====================

The GIST pipeline and this documentation have so far been maintained by Adrian Bittner. Having left academia in 
September 2021, further support and maintenance of the software will be limited, but in case of doubt you can get in 
contact via :email:`gistpipeline@gmail.com<gistpipeline@gmail.com>`

