.. _introduction:

=====================
Overview & Features
=====================

Overview
""""""""""""""""""""""""""

The GIST is a convenient, all-in-one framework for the scientific analysis of fully reduced, (integral-field)
spectroscopic data. It is entirely written in Python3 and conducts all steps from the preparation of input data, over
the scientific analysis to the production of publication-quality plots.

In its default implementation, it extracts stellar kinematics, performs an emission-line analysis, derives star
formation histories and stellar population properties from full spectral fitting as well as via the measurement of
absorption line-strength indices. To this end, the GIST is exploiting the well-known pPXF and GandALF routines. In
addition, the framework is not specific to any instrument or analysis technique and provides easy means of modification
and further development, as of its modular code architecture. In fact, it is not only a neat combination of already
existing fitting routines, but a fully modular framework for the analysis of spectroscopic data in the context of a
variety of scientific objectives. 

The software further features the dedicated visualisation routine Mapviewer which has a sophisticated graphical user
interface. This allows the easy, fully-interactive plotting of all measurements, in particular maps, observed spectra,
fits, residuals, as well as star formation histories and the weight distribution of the models. 

An elaborate, Python-native parallelisation is implemented and tested on various machines from laptop to cluster scales. 

To date, the GIST framework has successfully been applied to both low and high-redshift data from MUSE, PPAK (CALIFA),
SINFONI, KCWI, and MaNGA, as well as to simulated data for HARMONI, WEAVE, and other artificial observations. 

|

.. figure:: ../../_images/flowchart.pdf
   :align: right
   :figwidth: 50%

This framework consists of a series of modules which conduct all necessary steps from the read-in of input data, over
its preparation and fitting, to the visualisation of the results.  The figure on the right illustrates the workflow of
the GIST pipeline. In summary, it conduct the follow steps: 

*Data Preparation*

 * Read-in of the reduced, science-ready spectral data
 * Apply a spatial mask
 * Spatially bin the data
 * Logarithmically rebin the spectra
 * Preparation of a stellar template library

*Main Analysis*

 * Derivation of stellar kinematics with pPXF
 * Derivation of stellar population properties with a regularised run of pPXF
 * Derivation of emission-line properties with GandALF
 * Derivation of line-strength indices and population properties

*Visualisation Options*

 * Simplify the inspection of the data products by providing the dedicated visualisation software :ref:`mapviewer`
 * Automatic generation of publication-quality plots during runtime


.. hint::
   This documentation uses data from the MUSE integral-field spectrograph to illustrate the capabilities of the GIST
   pipeline. Nonetheless, we highlight that this software is **not** specific to IFS data and can likewise be used to analyse
   data from multi-object or long-slit spectrograhs.


|

Core Design Principles
"""""""""""""""""""""""

The aim of this project is to provide a sophisticated spectral analysis pipeline that has the following properties: 

* **Convenience**:             It is convenient by providing an all-in-one framework from the read-in of the data, over the analysis, to the automatic production of publication-quality plots
* **Generality**:              It works well with different instruments in the context of different projects/surveys
* **Modularity**:              It allows to turn individual analysis modules on/off or replace them without affecting the overall integrity of the analysis framework
* **Performance**:             It is equipped with a stable parallelisation and thus supports the processing of multiple spectra simultaneously 
* **Extensive Functionality**: It is extensive enough to cope with a variety of analysis procedures and their many different flavours 
* **Flexibility**:             It is flexible enough to work on bins, spaxels, as well as manually defined apertures
* **Intelligence**:            It automatically detects already available output and continues the analysis from there. It further allows the re-use of previously obtained results (e.g. the Voronoi binning) between similar analysis runs
* **Visualisation**:           It provides the dedicated visualisation software :ref:`mapviewer` with an advanced and fully interactive GUI, as well as automatically generated publication-quality plots
* **Clean Code**:              The code is clean and well-structured, making specific modifications by users easy
* **Expandable**:              The code is designed to simplify the implementation of additional modules or modification of the existing analysis framework
* **Ongoing Development**:     It is used by various collaborations and further analysis modules are in development

We emphasise that thanks to the very modular design of the pipeline, it is easy to modify, replace and expand parts
of this code, in order to meet the specific requirements of any project. For instance, it is easily possible to replace
the GandALF module by pyPARADISE to conduct the emission-line analysis. Similarly, one can add specific modules, or
simply replace a few functions, to e.g. facilitate an alternative binning scheme or emission-line subtraction technique.
This makes this pipeline a valuable tool for a large variety of different :ref:`projects`. 


|

Screenshots and Example Plots
""""""""""""""""""""""""""""""""

.. figure:: ../../_images/mapviewerScreenshot.png
   :figwidth: 100 %

   Screenshot of the visualisation software *Mapviewer*. Shown is the stellar velocity map of NGC1433 as well as the
   underlying spectra, fits and residuals. Latter can be plotted in the right-hand column of the software by simply
   performing a mouse-click on a particular position in the map. 

.. image:: ../../_images/NGC1433_ppxf.pdf
   :width: 49 %

.. image:: ../../_images/NGC1433_lambdaR.pdf
   :width: 49 %

*Example plots of the galaxy NGC1433 as produced by the pipeline. Shown are maps of the stellar kinematics (left plot)
and the projected, specific angular momentum per bin (lambda_r, right plot). The plotted quantities are displayed in the
upper left of each panel while the minimum and maximum displayed values are stated in the lower right.* 



|

.. _projects:

Projects
""""""""""""""""""
This code is already been used in the data analysis of the 
`TIMER <https://www.muse-timer.org>`_, 
`F3D <http://www.na.astro.it/Fornax3D/Fornax3D/Welcome.html>`_, and
`PHANGS <https://sites.google.com/view/phangs/home>`_ 
collaborations. 

.. image:: ../../_images/timer.png
   :width: 20 %
   :target: https://www.muse-timer.org

.. image:: ../../_images/phangs.png
   :width: 20 %
   :target: https://sites.google.com/view/phangs/home


|

