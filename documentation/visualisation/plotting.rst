.. _plotting:

===================
Plotting Routines
===================

The GIST pipeline produces publication-quality plots of relevant outputs automatically during runtime. Please find
examples of all available plots below. 

In addition, the same plotting routines the pipeline calls during runtime can also be executed independently from the
pipeline, in particular to adapt the minimum/maximum values displayed in the maps as well as the isophotes. Upon
execution of the plotting routines, you will be prompted to input the minimum level of the isophotes, as well as all
minimum/maximum values to be displayed for each quantity. Once you entered all values, the routines will display a
preview of the plot that will be produced. Close this plot and state whether you want to save it, or restart with
entering new values. To keep the values from the previous iteration, just hit ENTER. Iterate until you are satisfied
with your plot. 

Please note that negative tick labels and axis labels might appear shifted in vertical direction during the preview of
the plot.  Unfortunately, this behaviour is a known Matplotlib bug. However, when saving the plot to a pdf-file this
behaviour should disappear naturally.  If the problem does not disappear, you can try to use ``import matplotlib;
matplotlib.use('pdf')`` manually *BEFORE* importing any other Matplotlib module! Note that in this case the plot cannot
be displayed but only saved to a pdf-file. 

Also note that the default version of the plotting routines require a LaTeX installation.

Please find below an overview of the available plotting routines.



|

Stellar Kinematics
-----------------------

:: 

   gistPlot_kin -r PATH -f FLAG

with ``PATH`` being the absolute path to the output directory of the run and flag defining the module, e.g. "KIN" or "SFH". 

.. figure:: ../../_images/NGC1433_ppxf.pdf
   :figwidth: 100%

|

Lambda Parameter
----------------------

::

   gistPlot_lambdar -r PATH

with ``PATH`` being the absolute path to the output directory of the run. 

.. figure:: ../../_images/NGC1433_lambdaR.pdf
   :figwidth: 100%

|

Emission Line Properties
--------------------------

::

   gistPlot_gas -r PATH -l LEVEL -e LINE -a AON_THRESHOLD

with ``PATH`` being the absolute path to the output directory of the run and ``LEVEL`` being either "BIN" or "SPAXEL".
``LINE`` is the name of the fitted emission line, as used in the columns of the ``_gas-*.fits`` output file.
``AON_THRESHOLD`` specifies the minimum amplitude-over-noise ratio for the emission line to be displayed in the map. 

.. figure:: ../../_images/NGC1433_gandalf-Ha_V_SPAXEL.pdf
   :figwidth: 100%

|

Stellar Population Properties
--------------------------------

::

   gistPlot_sfh -r PATH -f FLAG

with ``PATH`` being the absolute path to the output directory of the run and ``FLAG`` defining the module, e.g. "SFH" or "LS". 

.. figure:: ../../_images/NGC1433_spp_ls.pdf
   :figwidth: 100%

|

Line Strength Indices
---------------------------

::

   gistPlot_ls -r PATH -s RESOLUTION

with ``PATH`` being the absolute path to the output directory of the run and ``RESOLUTION`` defining if the results
obtained at the original resolution of the data ("ORIGINAL") or after the convolution ("ADAPTED") should be displayed. 

.. figure:: ../../_images/NGC1433_ls_AdapRes.pdf
   :figwidth: 100%



|

