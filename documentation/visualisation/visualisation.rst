.. _visualisation: 

**********************
Visualisation Options
**********************

In this section I describe the visualisation options that the GIST pipeline provides. In particular, the pipeline is 
equipped with the dedicated visualisation software *Mapviewer* that facilitates a graphical-user interface. In addition, 
publication-quality plots are produced during runtime of the pipeline. 

.. toctree::
   :maxdepth: 1
   
   mapviewer
   plotting

