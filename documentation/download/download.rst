.. _download: 

***********************
Download & News
***********************

On this page you can download the main version of the pipeline. Provided are links to the public GitLab repository of
the GIST, its PyPi repository, and an example working directory. Latter includes an example IFS-cube, pre-defined
configuration files, the necessary directory structure, and works readily within the pipeline framework. Please see
:ref:`tutorial` and :ref:`installation` for further details. 



**Download**

* `PyPi Repository <https://pypi.org/project/gistPipeline/>`_
* `GitLab Repository <https://gitlab.com/abittner/gist-development>`_
* :download:`Example Working Directory <../../_downloads/gistTutorial.tar.gz>`



**Release Notes**

.. literalinclude:: CHANGELOG


|   

.. toctree::
  :hidden:
  
  self
  newInVersion3
