Changes in Version 3.0
=========================

Although version 3.0 of GIST has only few new features, it is based on a substantial revision and re-structuring of its
entire source code. The distinct modules of the framework are now clearly visible in the source code, separated in
singular routines in different subdirectories (as previously for the read-in routines).  This makes it even more
straightforward for the user to modify the GIST, and add alternative routines. 

The ``MasterConfig`` has been changed accordingly, now also being separated in various sections, each corresponding to
one module. The configuration parameters given in each section are only used by the respective module and do not
interact with any other part of the GIST framework. Therefore, configuration parameters can simply be
added/changed/removed, as required by the user-developed modules without affecting any other part of the GIST.  Note
that the paths to all configuration files now specifically need to be defined in the ``MasterConfig`` file.
Configuration files in the output directory or the templates configuration files provided in the tutorial will not be
used by default.

In addition, it is not required to use the standardised GIST working directory structure. Instead, the paths to the
configuration, input, output, and spectral template directories are defined in a dedicated ``defaultDir`` file and the
relevant files specifically defined in the ``MasterConfig`` file. 

Also the Mapviewer has been fundamentally revised. The menu bar is now dynamically generated from the data available in
the output files. In particular, if a user-defined routine adds additional columns to the output files, this data will
readily be displayed by the Mapviewer. 



|

Other than that, there are the following relevant changes: 

* The pPXF implementation of the ``stellarKinematics`` module now supports the measurement of extinctions, based on the
  pPXF keyword ``REDDENING``.
* The output file ``-goodpix.fits`` of all analysis modules has been moved into extension 3 of the ``-bestfit.fits``
  file. 
* In the output tables, now only quantities that were actually measured are saved, e.g. ``_kin.fits`` will not contain
  columns ``H5`` and ``H6`` if only 4 kinematic moments were extracted. 
* The main output of the ``emissionLines`` module (``_gas_*.fits``) was substantially restructured. Instead of the
  complicated structure used in previous versions, this file now simply contains one column for each velocity, sigma,
  amplitude, flux, and amplitude-over-noise ratio for each emission-line. This should significantly simplify the access
  to the data. 
* The ``spatialMasking`` module now supports the usage of spatial masks defined in mask files. These should have the
  same spatial extent as the cube in consideration, and contain an array in extension 1 that defines spaxels with value
  0 as unmasked, while spaxels with value 1 are considered masked.  In addition, the module now clear separates the
  different masks (i.e. defunct spaxels, signal-to-noise threshold, mask file) and saves them in a dedicated output file
  (``_mask.fits``). 
* A number of configuration and output files have been slightly renamed in the process, but these files still function
  as before.
* Modify ``starFormationHistories`` and ``lineStrengths`` modules to only use emission-subtracted spectra in the
  analysis if those are actually available. If the ``emissionLines`` module was not executed (e.g. because there is no
  emission in the galaxy) those modules continue the analysis with the observed spectra. 
* The ``MasterConfig`` file is equipped with the new parameters ``OW_CONFIG`` and ``OW_OUTPUT``. If ``OW_CONFIG`` is
  set, the analysis will be performed even if the configuration parameters in the current ``MasterConfig`` are not
  consistent with those saved in the output directory in ``CONFIG``. If ``OW_OUTPUT`` is set, all steps of the analysis
  are performed, regardless of any output files that might already be in the output directory. Output in the output
  directory will be overwritten. 
* The ``USED_PARAMS.fits`` was replaced by the ``CONFIG`` file. This file saved the used configuration parameters in a
  YAML format.  



**Of course, feedback, ideas, and suggestions are very welcome!**



|

