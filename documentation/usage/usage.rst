.. _usage: 

*****************
Usage & Tutorial
*****************

Here we provide an introduction into the usage of the pipeline.  First, I explain the basic steps of the
:ref:`installation` process, before providing a :ref:`tutorial`. The following subsections :ref:`configuration`,
:ref:`running` and :ref:`output` provide a more throughout overview of the features and capabilities of this extensive
code. It is highly recommended to read through these sections. 

.. toctree::
   :maxdepth: 1
   
   installation
   tutorial
   configuration
   running
   output


