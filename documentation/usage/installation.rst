.. _installation:


Installation
======================

Please download the source code in :ref:`download` and run from within the source code directory::

   python setup.py install

Alternatively, you can directly download and install the GIST from PyPi::
   
   pip install gistPipeline

**We strongly recommend to install the GIST pipeline in a separate and new conda environment, using Python3.6!** For
further instructions on the use and management of conda environments, please see the `Conda Documentation
<https://conda.io>`_.



|

Analysis Software
-------------------
The installation includes suitable versions of the required scientific software, in particular pPXF
(see Cappellari et al. `2004 <https://ui.adsabs.harvard.edu/?#abs/2004PASP..116..138C/abstract>`_, `2017
<https://ui.adsabs.harvard.edu/?#abs/2017MNRAS.466..798C>`_), and the Voronoi binning method (see 
`Cappellari & Copin 2003 <https://ui.adsabs.harvard.edu/?#abs/2003MNRAS.342..345C>`_). 
Implementations of the pyGandALF (see `Sarzi et al. 2006
<https://ui.adsabs.harvard.edu/#abs/2006MNRAS.366.1151S/abstract>`_; `Bittner et al. 2019
<https://ui.adsabs.harvard.edu/abs/2019arXiv190604746B/abstract>`_) and line strength measurement routines (see `Kuntschner et al. 2006
<https://ui.adsabs.harvard.edu/#abs/2006MNRAS.369..497K/abstract>`_; `Martin-Navarro et al. 2018
<https://ui.adsabs.harvard.edu/#abs/2018MNRAS.475.3700M/abstract>`_) are distributed with this software package. 

.. warning::
   Please do not forget to cite the publications of the corresponding analysis techniques if you use them for your
   research!

|


Compatible Python Versions
---------------------------
The pipeline is designed to run with `Python 3 <https://www.python.org>`_.  As many Python packages, in particular pPXF
and the Voronoi binning methods, do not support Python 2 anymore the GIST pipeline can only be executed with Python 3. 



|

Parallelisation
--------------------------
The parallelisation of the pipeline uses the ``multiprocessing`` module of Python's Standard Library. In particular, it uses
``multiprocessing.Queue`` and ``multiprocessing.Process`` providing a maximum of stability and control over the parallel
processes.  In addition, the threading/parallelisation of other Python native modules, such as NumPy or SciPy, is
suppressed. Thus, the number of active processes should never exceed the number of cores defined in the configuration
file, nor should any process be able to claim more than 100% CPU usage. 

The drawback of the Python multiprocessing module is that it does not natively support the use of multiple nodes on
large computing clusters. However, at this point the use of one node (with e.g. 32 cores) should be sufficient for most
kinds of analysis. Implementing a distributed memory parallelisation in the GIST pipeline is nonetheless a long-term
objective. 

The implemented parallelisation has been tested on various machines: This includes hardware from laptop up to
cluster systems, as well as Linux and MacOS operating systems. 

|

