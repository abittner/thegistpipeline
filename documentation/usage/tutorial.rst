.. _tutorial:

Tutorial: How to get started
============================

This pipeline is a very extensive tool, providing various techniques which can be called in different flavours.  The
next sections provide a detailed explanation on the :ref:`configuration`, :ref:`running` and :ref:`output` of the code.
To help you getting started with the GIST pipeline, we provide an easy tutorial below. 


Step 1
----------------

Please download the source code in :ref:`download` and run from within the source code directory::

   python setup.py install

Alternatively, you can directly download and install the GIST from PyPi::
   
   pip install gistPipeline

**We strongly recommend to install the GIST pipeline in a separate and new conda environment, using Python3.6!** For
further instructions on the use and management of conda environments, please see the `Conda Documentation
<https://conda.io>`_.


Step 2
----------------
Download the "Example Working Directory" from :ref:`download`. This tarball consists of an example setup to run the
pipeline with. In particular, it contains a GIST working directory, including an example IFS
cube (denoted ``NGC0000.fits``), a spectral template library (E-Miles, Kroupa revised, base-Fe; see Vazdekis et al.
`2015 <https://ui.adsabs.harvard.edu/#abs/2015MNRAS.449.1177V>`_ and `miles.iac.es <http://miles.iac.es>`_), and
templates of all necessary configuration files. 

Download the tarball and place it in a directory of your choice. We will refer to this directory as ``$WORKING_DIR`` in
the following. 


Step 3 
-----------------
Now you are already ready to run the pipeline. From within ``$WORKING_DIR`` the GIST pipeline is executed with the command::

   gistPipeline --config configFiles/MasterConfig --default-dir configFiles/defaultDir

The pipeline will start by printing the settings, and immediately continue with the preparation of the data (reading the
cube, generating the Voronoi bins, log-rebinning the spectra). Subsequently, the pipeline continues to run the ``stellarKinematics`` module.
It informs you about every step that is conducted by printing a message/progress bar in the terminal, as well as by
saving this information in a ``LOGFILE``. 

Following the example from above, the output will be saved in ``$WORKING_DIR/results/NGC0000_Example/``.  The most
important output files are:

* ``LOGFILE``: The logfile
* ``NGC0000_table.fits``: A table containing all information to reconstruct the used Voronoi binning
* ``NGC0000_BinSpectra.fits``: A table containing the spatially binned spectra
* ``NGC0000_kin.fits``: A table containing the stellar kinematics

For a full explanation of all produced output files see :ref:`modules`. 



|

Next Steps
***********************
After your first successful run of the pipeline, you will certainly want to move forward to explore the full capability
of this tool. Below, I provide a few ideas on how to move ahead. 


Look at the output
"""""""""""""""""""""""
Have a closer look at the output files, get a feeling for the information that is there and how it is saved.  Also look
at the plots, and try out the :ref:`mapviewer` (To be fair, the plots and the *Mapviewer* are way more impressive once
you have run the pipeline on a full cube instead of the tiny example cube). 


Edit the Config-file
"""""""""""""""""""""""
In the next step, you should have a closer look at the master configuration file
(``$WORKING_DIR/configFiles/MasterConfig``). Try to turn on the ``emissionLines`` and the ``starFormationHistories``
module and restart the pipeline as above in Step 3 - the pipeline automatically detects that the stellar kinematics
results are already available and only performs the other analysis steps. 

In order to start a new analysis run, just add another line in the master configuration file with a new ``RUN_ID``. Of
course the output directory will automatically change accordingly with the ``RUN_ID``. 

You should also start to play around with the other parameters in the Config-file. Please note that any changes of the
parameters (unless those denoted *Main Switches*) will require that you to start a new analysis-run with a new
``RUN_ID``, unless you set ``OW_CONFIG`` and ``OW_OUTPUT`` to *True* such that previously used configurations and outputs
are overwritten. 


Use your own cube
"""""""""""""""""""""""
Once you feel comfortable with the usage of the pipeline, try to run your own cube. Simply place the cube in
``$WORKING_DIR/inputData/`` and define a new analysis run in the master configuration file. 


Read the other sections 
"""""""""""""""""""""""
Eventually, you should read through the next sections (:ref:`configuration`, :ref:`running` and :ref:`output`) to get a
proper overview of how to configure the pipeline and become aware of all of its capabilities.



|

