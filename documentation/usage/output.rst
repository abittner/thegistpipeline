.. _output: 

Output
==============


**Mapping bins to spaxels:**
The file ``_table.fits`` contains all necessary information to reconstruct the spatial binning and maps bins to
spaxels and vice versa. In particular, this file contains one line per spaxel and states the BIN_ID as well as spatial
coordinates of every spaxel. 

In all output files that contain one row per bin, the row number corresponds to the BIN_ID given in
``_table.fits``. In output files consisting of one row per spaxel, the rows are sorted in the same way as in
``_table.fits`` so that these tables can be matched one-to-one. 


**Description of output files:**
A detailed description of all output files and their contents is provided in the context of the respective GIST modules
(see :ref:`modules`).



|

