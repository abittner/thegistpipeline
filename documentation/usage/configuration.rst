.. _configuration:

Configuration
===================


Working Directory
-------------------

While the GIST pipeline is installed in the system and can be executed from anywhere, directories containing the input,
output, configuration, and spectral templates have to be defined. In the example working directory this is done with the
file ``defaultDir`` in which the absolute paths to these directories should be defined. The path to this file needs to be
supplied to the GIST pipeline with the command-line argument ``default-dir`` (see :ref:`running`).

An overview of these directories and some important files is provided below
::

   workingDirectory/
   │ 
   ├─ inputData/
   │   └─ NGC0000.fits
   │ 
   ├─ spectralTemplates/
   │   ├─ MILES/
   │   └─ MILES_KB_LIS8.4.fits
   │ 
   ├─ configFiles/
   │  ├─ MasterConfig
   │  ├─ defaultDir
   │  ├─ lsf_MUSE-WFM
   │  ├─ lsf_MILES
   │  ├─ emissionLines.config
   │  ├─ lsBands.config
   │  ├─ specMask_KIN
   │  └─ specMask_SFH
   │
   └─ results/



The directory ``inputData/`` contains all the input data. Which input data is used by the pipeline is stated in
``MasterConfig`` (see below). 

The directory ``results/`` contains the outputs of the GIST pipeline. The results from different runs of the pipeline
are located in different subdirectories of ``results/``. The name of these subdirectories is specified in
``MasterConfig`` (see below). 

The directory ``spectralTemplates/`` can contain various spectral template libraries in different subdirectories (e.g.
``MILES/``). In addition, if one intends to match a set of measured absorption line strength indices to single stellar
populations (SSP), it is necessary to supply a fits-file with the same name as the spectral template library (e.g.
``MILES.fits``). This file has to provide the predicted line strength indices for the various SSPs in the library.

The directory ``configFiles/`` contains all relevant configuration files. These are in particular: 

* ``MasterConfig``: This file provides the main configuration of the pipeline. It defines which input data and spectral
  template library is used and which analysis modules will be executed. It further specifies the main parameters for
  this analysis, for instance the target signal-to-noise ratio of the Voronoi binning and the rest-frame wavelength
  range in consideration. The main configuration file is passed to the pipeline with the command-line argument
  ``--config`` (see :ref:`running`).
* ``defaultDir``: This file defines the absolute paths to the input, output, configuration, and spectral template
  directories. It must be passed to the pipeline with the command-line argument ``--default-dir`` (see :ref:`running`).
* ``lsf_[Instrument]``: This file defines the line-spread-function of the used instrument. 
* ``lsf_[TemplateLibrary]``: This file defines the line-spread-function of the used template library. 
* ``emissionLines.config``: This file specifies the various emission-lines to be fitted or masked with the
  ``emissionLines`` module. Note that this configuration file only affects the ``emissionLines`` module. 
* ``lsBands.config``: This file states the wavelength bands of the line strength indices measured by the ``lineStrengths`` module. It
  further defines which indices to consider for the conversion of line strength indices to stellar population
  properties. Note that this configuration file only affects the ``lineStrengths`` module. 
* ``specMask_KIN``: This files defines spectral regions to be masked during the ``stellarKinematics`` analysis. 
* ``specMask_SFH``: This files defines spectral regions to be masked during the ``starFormationHistories`` analysis. 

The files ``emissionLines.config``, ``lsBands.config``, ``specMask_KIN`` and ``specMask_SFH`` can be different for
different runs of the pipeline.  Therefore, the files provided in ``configFiles/`` should only be considered as
templates. **The used versions of these files always need to be defined in the corresponding parameters in**
``MasterConfig``.

A detailed description of each configuration file is provided below. 



|

The Configuration Files
-------------------------

MasterConfig
"""""""""""""""""""
The ``MasterConfig`` file provides the main configuration for the pipeline. It defines which modules to execute, and
specifies the main parameters for the analysis. The main configuration file is passed to the pipeline with the
command-line argument ``--config`` (see :ref:`running`).

**The paths to all configuration files now need to be defined in the** ``MasterConfig`` **file. The configuration files
in** ``configFiles`` **or the current output directory are not anymore used by default!** 

| File location: Given with the command-line argument ``--config``
| Download file: :download:`MasterConfig <../../_downloads/MasterConfig.txt>`


**General**

* ``RUN_ID``            Name of the analysis run. A subdirectory of this name within the output directory will be created. This identifier further serves as a prefix to all output files. **Underscores in this parameter should be avoided!**
* ``INPUT``             Input file for this analysis run. The specified path is relative to the input path given in defaultDir. 
* ``OUTPUT``            Output directory. The output of this run will be collected in a subdirectory named RUN_ID. The specified path is relative to the output path given in defaultDir. 
* ``REDSHIFT``          Initial guess on the redshift of the system [in z]. Spectra are shifted to rest-frame, according to this redshift. 
* ``PARALLEL``          Use multiprocessing [True/False]
* ``NCPU``              Number of cores to use for multiprocessing
* ``LSF_DATA``          Path of the file specifying the line-spread-function of the observational data. The specified path is relative to the configDir path in defaultDir.
* ``LSF_TEMP``          Path of the file specifying the line-spread-function of the spectral templates. The specified path is relative to the configDir path in defaultDir.
* ``OW_CONFIG``         Ignore configurations from previous runs which are saved in the CONFIG file in the output directory [True/False]
* ``OW_OUTPUT``         Overwrite any output files already present in the current output directory [True/False]


**READ_DATA**

* ``METHOD``            Name of the routine in readData/ (without .py) to be used to read-in the input data.
* ``DEBUG``             Switch to activate debug mode [True/False]: Pipeline runs on one, central line of pixels. Keep in mind to clean output directory after running in DEBUG mode!
* ``ORIGIN``            Origin of the coordinate system in pixel coordinates: x,y (Indexing starts at 0).
* ``LMIN_TOT/LMAX_TOT`` Spectra are shortened to the rest-frame wavelength range defined by LMIN_TOT and LMAX_TOT. Note that this wavelength range should be longer than all other wavelength ranges supplied to the modules [in Angst.]
* ``LMIN_SNR/LMAX_SNR`` Rest-frame wavelength range used for the signal-to-noise calculation [in Angst.]


**SPATIAL_MASKING**

* ``METHOD``            Name of the routine in spatialMasking/ (without .py) to perform the tasks. Set 'False' to turn off module. Set 'default' to use the standard GIST implementation.
* ``MIN_SNR``           Spaxels below the isophote level which has this mean signal-to-noise level are masked. 
* ``MASK``              File containing a spatial mask (Set 'False' to not include a file). 
  

**SPATIAL_BINNING**

* ``METHOD``            Name of the routine in spatialBinning/ (without .py) to perform the tasks. Set 'False' to turn off module. Set 'voronoi' to use the standard GIST implementation, exploiting the Voronoi tesselation routine of Cappellari & Copin (2003). 
* ``TARGET_SNR``        Target signal-to-noise ratio for the Voronoi binning
* ``COVARIANCE``        Correct for spatial correlations of the noise during the Voronoi binning process according to the empirical equation SNR /= 1 + COVAR_VOR * np.log10(NSPAXEL) with NSPAXEL being the number of spaxels per bin (see e.g. Garcia-Benito et al. 2015). 


**PREPARE_SPECTRA**

* ``METHOD``            Name of the routine in prepareSpectra/ (without .py) to perform the tasks. Set 'False' to turn off module. Set 'default' to use the standard GIST implementation.
* ``VELSCALE``          Spectral sampling of the logarithmically rebinned spectra [in km/s]; e.g. velscale = dLambda*C / mean(wave), with the spectral sampling of the linearly binned spectra dLambda, the speed of light C, and the mean wavelength of the spectrum mean(wave). 


**PREPARE_TEMPLATES**

* ``METHOD``            Name of the routine in prepareTemplates/ (without .py) to perform the tasks. Set 'False' to turn off module. Set 'miles' to use the standard GIST implementation for handling Miles/EMiles templates.
* ``LIBRARY``           Directory containing the spectral templates. The specified path is relative to the templateDir path in defaultDir.
* ``NORM_TEMP``         Normalise the spectral template library to obtain light- or mass-weighted results [LIGHT / MASS]


**KIN**

* ``METHOD``            Name of the routine in stellarKinematics/ (without .py) to perform the tasks. Set 'False' to turn off module. Set 'ppxf' to use the standard GIST implementation, exploiting the pPXF routine of Cappellari & Emsellem (2004).
* ``SPEC_MASK``         File to define wavelength ranges to be masked during the stellar kinematics fit. The specified path is relative to the configDir path in defaultDir. 
* ``LMIN / LMAX``       Rest-frame wavelength range used for the stellar kinematics analysis [in Angst.]
* ``SIGMA``             Initial guess of the velocity dispersion of the system [in km/s]
* ``MOM``               Number of kinematic moments to be extracted
* ``ADEG``              Degree of the additive Legendre polynomial. Set '-1' to not include any additive polynomials
* ``MDEG``              Degree of the multiplicative Legendre polynomial. Set '0' to not include any multiplicative polynomials
* ``REDDENING``         Initial guess on the stellar reddening E(B-V), in order to measure the stellar reddening. Note: This cannot be used together with multiplicative polynomials. 
* ``MC_PPXF``           Number of Monte-Carlo simulations to extract errors on the stellar kinematics. Formal errors are saved in any case. 


**GAS**

* ``METHOD``            Name of the routine in emissionLines/ (without .py) to perform the tasks. Set 'False' to turn off module. Set 'gandalf' to use the standard GIST implementation, exploiting the pyGandALF routine of Sarzi et al. (2006). 
* ``LEVEL``             Set 'BIN' to perform the analysis bin-by-bin, 'SPAXEL' for a spaxel-by-spaxel analysis, and 'BOTH' to perform a spaxel-by-spaxel analysis that is informed by a previous bin-by-bin analysis. 
* ``LMIN / LMAX``       Rest-frame wavelength range used for the emission-line analysis [in Angst.]
* ``ERRORS``            Derive errors on the emission-line analysis (0 No / 1 Yes). Note: Due to limitations in pyGandALF, this is currently not possible. We expect a new pyGandALF version to be published soon.
* ``REDDENING``         Include the effect of reddening by dust in the pyGandALF fit. Put in the form 0.1,0.1 without any spaces. If you intend to use multiplicative polynomials instead, set REDDENING to 'False' and add a MDEG keyword in the GAS section to set the polynomial order. Additive polynomials cannot be used with pyGandALF.
* ``EBmV``              De-redden the spectra for the Galactic extinction in the direction of the target previously to the analysis. Use e.g. EBmV = A_v / 3.2
* ``EMI_FILE``          Emission line set-up file for pyGandALF. The specified path is relative to the configDir path in defaultDir. 


**SFH**

* ``METHOD``            Name of the routine in starFormationHistories/ (without .py) to perform the tasks. Set 'False' to turn off module. Set 'ppxf' to use the standard GIST implementation, exploiting the pPXF routine of Cappellari & Emsellem (2004). 
* ``LMIN / LMAX``       Rest-frame wavelength range used for the star formation histories analysis [in Angst.]
* ``SPEC_MASK``         File to define wavelength ranges to be masked during the star formation histories analysis. The specified path is relative to the configDir path in defaultDir. 
* ``MOM``               Number of kinematic moments to be extracted. If you use FIXED = True, this should be same number of moments used to extract the stellar kinematics before. Otherwise the parameter can be set independently. 
* ``MDEG``              Degree of the multiplicative Legendre polynomial. Set '0' to not include any multiplicative polynomials. Note that additive Legendre polynomials cannot be used for this module. 
* ``REGUL_ERR``         Regularisation error for the regularised run of pPXF. Note: Regularisation = 1 / REGUL_ERR
* ``NOISE``             Set a wavelength independent noise vector to be passed to pPXF. 
* ``FIXED``             Fix the stellar kinematics to the results obtained with the stellar kinematics module [True / False]. If 'False', please provide an initial guess on the velocity dispersion of the systems [in km/s] by adding the parameter SIGMA. 


**LS**

* ``METHOD``            Name of the routine in lineStrengths/ (without .py) to perform the tasks. Set 'False' to turn off module. Set 'default' to use the standard GIST implementation, exploiting the routines of Kuntschner et al. (2006) and Martin-Navarro et al. (2018).
* ``TYPE``              Set 'LS' to only measure line strength indices, or 'SPP' to also match these indices to stellar population properties. 
* ``LS_FILE``           File to define the wavelength band of the line strength indices to be measured. The specified path is relative to the configDir path in defaultDir. 
* ``CONV_COR``          Spectral resolution [in Angst.] at which the line strength indices are measured.
* ``SPP_FILE``          File providing predictions on line strength indices for a set of single stellar population models
* ``MC_LS``             Number of Monte-Carlo simulations in order to obtain errors on the line strength indices. Note: This must be turned on.
* ``NWALKER``           Number of walkers for the MCMC algorithm (used for the conversion of indices to population properties)
* ``NCHAIN``            Number of iterations in the MCMC algorithm (used for the conversion of indices to population properties)



|

defaultDir
""""""""""""""""""""""""

| File location: Specified with the command-line argument ``--default-dir``
| Download file: :download:`defaultDir <../../_downloads/defaultDir.txt>`

The file ``defaultDir`` defines the absolute paths to the directories containing the input, output, default
configuration, and spectral template files. The precise files used in an analysis run have to be defined in the
``MasterConfig``, but the path in ``MasterConfig`` is given relative to the default paths stated in ``defaultDir``. 

The ``defaultDir`` file must be specified with the command-line argument ``--default-dir`` (see :ref:`running`). 



|

lsf_*
""""""""""""""""""""""""

| File location: Defined in ``MasterConfig``
| Download file: :download:`lsf_MUSE-WFM <../../_downloads/lsf_MUSE-WFM.txt>`
| Download file: :download:`lsf_MILES    <../../_downloads/lsf_MILES.txt>`


The GIST pipeline employs two LSF configuration files: One defining the LSF of the used instrument (e.g.
``lsf_MUSE-WFM``) and one defining the LSF of the spectral templates (e.g. ``LSF-Config_MILES``).  The file has two
columns: The first column contains the wavelength in Angstrom while the second column specifies the FWHM of the
instrument/template at the given wavelength.

The pipeline creates a linear interpolation function using ``scipy.interpolate.interp1d`` between wavelength and FWHM,
so that the FWHM can be reconstructed at any given wavelength.  All spectral templates are broadened according to the
difference in resolution between templates and observed data prior to any fitting attempt. 



|

specMask_KIN
"""""""""""""""""""""""""""

| File location: Defined in ``MasterConfig``
| Download file: :download:`specMask_KIN <../../_downloads/specMask_KIN.txt>`

The file ``specMask_KIN`` defines wavelength ranges that are masked during the run of the ``stellarKinematics`` module. The file has
three columns: The first and the second column state the wavelength (in restframe) and width of the spectral masks in units of
Angstrom. Comments can be saved in a third column. 

In order to mask sky-lines, please specify ``sky`` in the third column. 

Note that this file only affects the ``stellarKinematics`` module. 


|

specMask_SFH
""""""""""""""""""""""""""

| File location: Defined in ``MasterConfig``
| Download file: :download:`specMask_SFH <../../_downloads/specMask_SFH.txt>`

The file ``specMask_SFH`` defines wavelength ranges that are masked during the run of the ``starFormationHistories`` module. The
file has three columns: The first and the second column state the wavelength and width of the spectral masks in units of
Angstrom. Comments can be saved in a third column. 

In order to mask sky-lines, please specify ``sky`` in the third column. 

Note that this file only affects the ``starFormationHistories`` module. 


|

lsBands.config
""""""""""""""""""""""""
 
| File location: Defined in ``MasterConfig``
| Download file: :download:`lsBands.config <../../_downloads/lsBands.config>`

The file ``lsBands.config`` defines the wavelength bands of the line strength indices to be measured. More specifically,
column ``b3`` and ``b4`` state the main band while ``b1`` and ``b2``, as well as ``b5`` and ``b6`` determine the
corresponding side bands. Column ``b7`` states whether the index is an atomic (1) or a molecular (2) index. In addition,
the names of the indices are provided. These names will also represent the column names in the output table
``NGC0000_ls.fits`` extension 1. 

The column ``spp`` specifies which indices will be considered in the conversion of line strength indices to
SSP-equivalent population properties (1 used / 0 ignored). 

Note that this configuration file only affects the ``lineStrengths`` module. 


| 

emissionLines.config
""""""""""""""""""""""""

| File location: Defined in ``MasterConfig``
| Download file: :download:`emissionLines.config <../../_downloads/emissionLines.config>`

The ``emissionLines.setup`` specifies the various emission-lines to be fitted with the ``emissionLines`` module.  

This file is structured as follows:

- ``i_line``: Index of the line
- ``name``: Defines the name of the emission line; Set ``sky`` to mask sky-lines. 
- ``lambda``: Defines the wavelength of the emission line
- ``action``: Defines what should be done with the emission line: Set ``m`` to mask, ``f`` to fit, and ``i`` to ignore the emission-line. 
- ``l-kind``: Define whether the line is a singlet (l) or part of a doublet (d25). Latter number specifies the index of the other doublet line. 
- ``A_i``: Relative amplitudes of doublet lines. Set to 1.0 for singlet lines. 
- ``V_g``: Initial guess on the velocity.
- ``sig_g``: Initial guess on the velocity dispersion. For masked lines this defines the width of the spectral mask. 
- ``fit-kind``: Kind of the fit to be performed: Set "f" to fit the line freely. Set "t17" to tie the kinematics of the line to those of the line with ``i_line`` 17. Set "h" (hold) to fix the kinematics to their input values. 
- ``AoN``: Minimum amplitude-over-noise ratio to consider the emission-line detection significant. Note that this affects the determination of emission-subtracted spectra. 

Note that this configuration file only affects the ``emissionLines`` module.



|

