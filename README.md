The Documentation of the GIST Framework
=======================================

This repository contains the documentation webpage of the GIST Framework.

The documentation is build as a Python Sphinx project, and deployed via GitLab
Pages. To access older version of this documentation, clone the repository and
checkout to any previous commit. Main GIST releases are marked with tags.

You can access the GIST source code and documentation at: 

* GIST Documentation: https://abittner.gitlab.io/thegistpipeline
* GitLab Repository: https://gitlab.com/abittner/gist-development
* PyPi Repository: https://pypi.org/project/gistPipeline
